package vroom.vrroommenu;

import android.app.Fragment;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * settings frag.
 */
public class SettingsFragment extends Fragment {

    Button btn;
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SettingsFragment newInstance(int sectionNumber) {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public SettingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_config, container, false);
        btn = (Button) rootView.findViewById(R.id.button_save_config);

        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View w) {
                System.out.println("button save clicked");
                TextView temperatureTextView = (TextView) getView().findViewById(R.id.editText);
                Spinner musicSpinner = (Spinner) getView().findViewById(R.id.spinner);
                TextView lightTextView = (TextView) getView().findViewById(R.id.editText4);
                TextView colorTextView = (  TextView) getView().findViewById(R.id.edittextLightcolor);
                TextView IpTextView = (  TextView) getView().findViewById(R.id.editTextIP);
                TextView RoomnameTextView = (  TextView) getView().findViewById(R.id.editTextRoomname);

                //save in file key value pairs
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();

                editor.putString("temperature", temperatureTextView.getText().toString());
                editor.putString("light", lightTextView.getText().toString());
                editor.putString("colorString", colorTextView.getText().toString());
                editor.putString("IpString", IpTextView.getText().toString());
                editor.putString("RoomnameString", RoomnameTextView.getText().toString());
                editor.putInt("musicINT", musicSpinner.getSelectedItemPosition());

                editor.commit();
                Button button = (Button) w;
                button.setText("saved");

            }
        });

        setSpinnerContent(rootView);
        return rootView;
    }

    private void setSpinnerContent(View view) {
        System.out.println("start onCreate Fragment");
        //fill dropdown select field
        String[] music_styles = {"Rock", "Pop", "Metal"};
        ArrayAdapter<String> stringArrayAdapter =
                new ArrayAdapter<String>(
                        getActivity(),
                        android.R.layout.simple_spinner_dropdown_item,
                        music_styles);

        Spinner spinner =
                (Spinner) view.findViewById(R.id.spinner);
        spinner.setAdapter(stringArrayAdapter);


        //get stored values
        SharedPreferences sharedPref = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        String temperature = "";
        String Ip = "";
        String Roomname = "";
        Integer music = 0;
        String light = "";
        String color = "0 0 0";
        temperature = sharedPref.getString("temperature", temperature);
        light = sharedPref.getString("light", light);
        color = sharedPref.getString("colorString", color);
        Ip = sharedPref.getString("IpString", Ip);
        Roomname = sharedPref.getString("RoomnameString", Roomname);
        music = sharedPref.getInt("musicINT", music);

        if (Ip != "") {
            TextView IpTextView = (TextView) view.findViewById(R.id.editTextIP);
            IpTextView.setText(Ip);
        } if (Roomname != "") {
            TextView RoomnameTextView = (TextView) view.findViewById(R.id.editTextRoomname);
            RoomnameTextView.setText(Roomname);
        }

        if (temperature != "") {
            TextView temperatureTextView = (TextView) view.findViewById(R.id.editText);
            temperatureTextView.setText(temperature);
        }

        if (light != "") {
            TextView lightTextView = (TextView) view.findViewById(R.id.editText4);
            lightTextView.setText(light);
        }


        if (color != null) {
             TextView colorTextView = (TextView) view.findViewById(R.id.edittextLightcolor);
             colorTextView.setText(color);
        }

        if (music != null) {
            // Button button =  (Button)findViewById(R.id.button);
            // button.setText(music.toString());
            spinner.setSelection(music);
        }
        System.out.println("end onCreate Fragment");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Config) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}