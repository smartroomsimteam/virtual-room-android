package vroom.vrroommenu;


import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.JsonReader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import android.os.Handler;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * connection info frag.
 */
public class ConnectionFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private Handler uiHandler;
    private TextView editText;

    Button btn;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ConnectionFragment newInstance(int sectionNumber) {
        ConnectionFragment fragment = new ConnectionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ConnectionFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_testfrag, container, false);
        btn = (Button) rootView.findViewById(R.id.button_transfer);

        //for view access in thread
        editText = (TextView) rootView.findViewById(R.id.editTextOutput);
        uiHandler = new Handler();

        btn.setOnClickListener(new View.OnClickListener() {
            String receiverPack = "";

            @Override
            public void onClick(View w) {
                try {
                    MulticastSocket multicastSocket = new MulticastSocket(3351);
                    multicastSocket.joinGroup(InetAddress.getByName("192.168.1.9"));
                    multicastSocket.send(new DatagramPacket("SRSIMSRV".getBytes(), 8));
                    DatagramPacket packetGR = new DatagramPacket(new byte[23], 23);
                    do {
                        multicastSocket.receive(packetGR);
                        receiverPack = new String(packetGR.getData());
                    } while (receiverPack.equals("SRSIMSRV"));
                } catch (Exception e) {

                }

                TextView multicastView = (TextView) getView().findViewById(R.id.editTextOutput);
                multicastView.setText(receiverPack.toString());

                //write stored data to as json strings
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                String temperature = "";
                Integer music = 0;
                String light = "";
                String Ip_temp ="";
                String Roomname = "";
                String color = "";
                temperature = sharedPref.getString("temperature", temperature);
                light = sharedPref.getString("light", light);
                color = sharedPref.getString("colorString", color);
                music = sharedPref.getInt("musicINT", music);
                final String Ip = sharedPref.getString("IpString", Ip_temp);
                Roomname = sharedPref.getString("RoomnameString", Roomname);

                String musicString = "";

                //map to Strings
                switch(music){
                    case 0:
                        musicString  = "rock";
                        break;
                    case 1:
                        musicString  = "pop";
                        break;
                    case 2:
                        musicString  = "metal";
                        break;
                    default:
                        musicString  = "rock";
                        break;
                }

                final String myJsonTemp = "{\"targetTemperature \": \" " + temperature.toString() + "  \"}";
                final String myJsonMusic = "{\"preferredMusic\":  \" " + musicString + " \"}";
                final String myJsonIntensity = "{\"targetBrightness\": \" " + light.toString() + "\"}";
                final String myJsonColor = "{\"targetColor\": \" " + color.toString() + " \"}";
                final String myRoomId = "{\"roomId\": \""+Roomname+"\"}";

                TextView testTextView = (TextView) getView().findViewById(R.id.editText);
                testTextView.setText(myJsonColor + myJsonIntensity + myJsonMusic + myJsonTemp);

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ServerConnection serverConnection = new ServerConnection(Ip);
                            serverConnection.sendPreference(myRoomId);
                            serverConnection.sendPreference(myJsonTemp);
                            serverConnection.sendPreference(myJsonMusic);

                            serverConnection.sendPreference(myJsonIntensity);
                            serverConnection.sendPreference(myJsonColor);

                            final String update = serverConnection.requestUpdate();
                            System.out.println(update);
                            JSONObject reader = new JSONObject(update);
                            if(update != null){
                                saveoutput(update);
                            }

                           // output_view = update;
                            uiHandler.post(new Runnable() {
                                public void run() {
                                    editText.setText(update);
                                }
                            });
                          // Thread.sleep(5000);
                          // serverConnection.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
        return rootView;
    }

    public void saveoutput(String jsonstring) throws JSONException {
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        // read Json
        JSONObject reader = new JSONObject(jsonstring);
        final String  brightness =  reader.getString("brightness");
        final String temperature = reader.getString("temperature");
        final String lightColor = reader.getString("lightColor");
        final String energyConsumption = reader.getString("energyConsumption");
        final String musicGenre = reader.getString("musicGenre");
        final String musicVolume = reader.getString("musicVolume");

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Config) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }
}