package vroom.vrroommenu;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.lang.Math;

import org.json.JSONException;
import org.json.JSONObject;
import android.os.Handler;

/**
 * main display frag.
 */
public class DisplayFragment extends Fragment {

    private Handler uiHandler;
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static DisplayFragment newInstance(int sectionNumber) {
        DisplayFragment fragment = new DisplayFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public DisplayFragment() {
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((Config) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        uiHandler = new Handler();

        View rootView = inflater.inflate(R.layout.fragment_display, container, false);

        setSpinnerContent(rootView);

        //write stored data to as json strings
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        String temperature = "";
        Integer music = 0;
        String light = "";
        String color = "";
        String Ip_temp ="";
        String Roomname = "";
        temperature = sharedPref.getString("temperature", temperature);
        light = sharedPref.getString("light", light);
        color = sharedPref.getString("colorString", color);
        music = sharedPref.getInt("musicINT", music);
        final String Ip = sharedPref.getString("IpString", Ip_temp);
        Roomname = sharedPref.getString("RoomnameString", Roomname);

        String musicString = "";

        //map to Strings
        switch(music){
            case 0:
                musicString  = "rock";
                break;
            case 1:
                musicString  = "pop";
                break;
            case 2:
                musicString  = "metal";
                break;
            default:
                musicString  = "rock";
                break;
        }

        final String myJsonTemp = "{\"targetTemperature \": \" " + temperature.toString() + "  \"}";
        final String myJsonMusic = "{\"preferredMusic\":  \" " + musicString + " \"}";
        final String myJsonIntensity = "{\"targetBrightness\": \" " + light.toString() + "\"}";
        final String myJsonColor = "{\"targetColor\": \" " + color.toString() + " \"}";
        final String myRoomId = "{\"roomId\": \""+Roomname+"\"}";


        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ServerConnection serverConnection = new ServerConnection(Ip);
                    serverConnection.sendPreference(myRoomId);
                    serverConnection.sendPreference(myJsonTemp);
                    serverConnection.sendPreference(myJsonMusic);

                    serverConnection.sendPreference(myJsonIntensity);
                    serverConnection.sendPreference(myJsonColor);

                    final String update = serverConnection.requestUpdate();
                    System.out.println(update);
                    JSONObject reader = new JSONObject(update);
                    if(update != null){
                        saveoutput(update,getView());
                    }

                    // Thread.sleep(5000);
                    // serverConnection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        return rootView;
    }


    private void setSpinnerContent(View view) {

        //get stored values
        SharedPreferences sharedPref = this.getActivity().getPreferences(Context.MODE_PRIVATE);
        String temperature = "";
        Integer music = 0;
        String light = "";

        String color = "#345435";
        temperature = sharedPref.getString("temperature", temperature);
        light = sharedPref.getString("light", light);
        color = sharedPref.getString("colorString", color);
        music = sharedPref.getInt("musicINT", music);

        if (temperature != "") {
            TextView temperatureTextView = (TextView) view.findViewById(R.id.textView);
            temperatureTextView.setText(temperature.toString() + "°");
        }

        if (light != "") {
            TextView lightTextView = (TextView) view.findViewById(R.id.textView4);
            lightTextView.setText(light.toString() + " lux");
        }

        if (color != null) {
            TextView colorTextView = (TextView) view.findViewById(R.id.textView2);
            colorTextView.setBackgroundColor(Color.parseColor(color.toString()));
            colorTextView.setText(color.toString());
        }

        if (music != null) {
            String musicText = "";
            TextView musicTextView = (TextView) view.findViewById(R.id.textView3);
            switch (music) {
                case 0:
                    musicText = "Rock";
                    break;
                case 1:
                    musicText = "Pop";
                    break;
                case 2:
                    musicText = "Metal";
                    break;
                default:
                    break;
            }
            musicTextView.setText(musicText);
        }

        System.out.println("end onCreate Fragment");
    }

    public void saveoutput(String jsonstring,View view) throws JSONException {

        final TextView editText6 = (TextView) view.findViewById(R.id.textView6);
        final TextView editText7 = (TextView) view.findViewById(R.id.textView7);
        final TextView editText8 = (TextView) view.findViewById(R.id.textView8);
        final TextView editText9 = (TextView) view.findViewById(R.id.textViewIp);
        final TextView editText10 = (TextView) view.findViewById(R.id.textView10);
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        // read Json
        JSONObject reader = new JSONObject(jsonstring);
        final String brightness = reader.getString("brightness");
        final String temperature = reader.getString("temperature");
        final String lightColor = reader.getString("lightColor");
        final String energyConsumption = reader.getString("energyConsumption");
        final String musicGenre = reader.getString("musicGenre");
        final String musicVolume = reader.getString("musicVolume");

        if(jsonstring != null) {
            // output_view = update;
            uiHandler.post(new Runnable() {
                public void run() {
                    editText7.setText("-> " + (Math.round(Double.valueOf(temperature)*100d) / 100d));

                    String colorRGB [] = lightColor.split(" ");

                    editText8.setText("-> #" + Integer.toHexString(Integer.parseInt(colorRGB[0]+colorRGB[1]+colorRGB[2])));
                    editText9.setText("-> " + musicGenre);
                    editText10.setText("-> " + brightness);
                    editText6.setText("Connected");
                }
            });
        }
        else{
            editText7.setText("");
            editText8.setText("");
            editText9.setText("");
            editText10.setText("");
            editText6.setText("Not Connected");
        }

    }

}